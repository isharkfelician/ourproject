package com.example.smartmirrorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnSignin;
    private EditText editTextEmail;
    private EditText editTextPwd;
    private TextView textViewSignup;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() != null){
            //start menu activity here
            finish();
            startActivity(new Intent(getApplicationContext(), Homedashboard.class));

        }

        editTextEmail = (EditText)findViewById(R.id.editTextEmail);
        editTextPwd = (EditText)findViewById(R.id.editTextPassword);
        btnSignin = (Button)findViewById(R.id.buttonSignin);
        textViewSignup = (TextView)findViewById(R.id.textViewSignUp);

        btnSignin.setOnClickListener(this);
        textViewSignup.setOnClickListener(this);

    }


    private void userLogin() {

        String email = editTextEmail.getText().toString().trim();
        String pwd = editTextPwd.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //empty mail
            Toast.makeText(this, "Empty fields!", Toast.LENGTH_SHORT).show();
            //return stop function from executing
            return;
        }
        if(TextUtils.isEmpty((pwd))){
            //empty password
            Toast.makeText(this, "Empty fields!", Toast.LENGTH_SHORT).show();
            //return stop function from executing
            return;
        }

        //if validations are ok show a progress bar
        //final MyProgressDialog myProgressDialog = new MyProgressDialog();
        //myProgressDialog.show();

        //Signing in..
        firebaseAuth.signInWithEmailAndPassword(email, pwd)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //myProgressDialog.dismiss();

                        if(task.isSuccessful()){
                            //take user to the menu
                            finish();
                            startActivity(new Intent(getApplicationContext(), Homedashboard.class));
                            Toast.makeText(LoginActivity.this, "Successfully Logged in.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Toast.makeText(LoginActivity.this, "Login error.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });



    }

    @Override
    public void onClick(View view) {
        if(view == btnSignin){
            userLogin();
        }
        if(view == textViewSignup){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

    }
}
