package com.example.smartmirrorapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import dmax.dialog.SpotsDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnRegister;
    private EditText editTextEmail;
    private EditText editTextPwd;
    private TextView textViewSignin;

    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){
            //start menu activity here
            finish();
            startActivity(new Intent(getApplicationContext(), Homedashboard.class));
        }

        btnRegister = (Button)findViewById(R.id.buttonRegister);
        editTextEmail = (EditText)findViewById(R.id.editTextEmail);
        editTextPwd = (EditText)findViewById(R.id.editTextPassword);
        textViewSignin = (TextView)findViewById(R.id.textViewSignin);

        btnRegister.setOnClickListener(this);
        textViewSignin.setOnClickListener(this);

    }

    private void registerUser(){
        String email = editTextEmail.getText().toString().trim();
        String pwd = editTextPwd.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //empty mail
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
            //return stop function from executing
            return;
        }
        if(TextUtils.isEmpty((pwd))){
            //empty password
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            //return stop function from executing
            return;
        }

        //if validations are ok show a progress bar


        firebaseAuth.createUserWithEmailAndPassword(email, pwd)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            //take user to the menu
                            finish();
                            startActivity(new Intent(getApplicationContext(), Homedashboard.class));
                            Toast.makeText(MainActivity.this, "Registration successful.", Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(MainActivity.this, "Please try again", Toast.LENGTH_SHORT).show();

                        }
                    }
                });


    }

    @Override
    public void onClick(View view){

        if(view == btnRegister) {
            registerUser();
        }
        if(view == textViewSignin){
            //open login activity
            startActivity(new Intent(this, LoginActivity.class));

        }

    }
}
