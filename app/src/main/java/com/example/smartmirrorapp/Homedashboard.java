package com.example.smartmirrorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Homedashboard extends AppCompatActivity implements OnClickListener{

    private ImageView logout;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homedashboard);

        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser() == null){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        FirebaseUser user = firebaseAuth.getCurrentUser();

        logout = (ImageView)findViewById(R.id.logoutView);
        logout.setOnClickListener(this);

        //OnClickButtonListner();
    }
    /*public void OnClickButtonListner(){
        logout = (ImageView)findViewById(R.id.logoutView);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));

            }
        });
    }*/

    @Override
    public void onClick(View view) {

        firebaseAuth.signOut();
        finish();
        startActivity(new Intent(this, LoginActivity.class));
    }}

